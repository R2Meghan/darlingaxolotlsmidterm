
$(document).ready(function () {

    $("#form").submit(function (e) {
        e.preventDefault();
    });

    $("#userError").hide();
    $("#userPass").hide();
    let usernameError = true;
    $("#userError").keyup(function () {
        validateUsername();
    });

    function validateUsername() {
        let usernameValue = $("#username").val();
        if (usernameValue.length == "") {
            $("#userError").show();
            $("#userPass").hide();
            document.getElementById('username').style.borderColor = "red"
            usernameError = false;
            return false;
        } else if (usernameValue.length < 5 || usernameValue.length > 11) {
            $("#userError").show();
            $("#userPass").hide();
            $("#userError").html("Username should be >=6 and <=10");
            document.getElementById('username').style.borderColor = "red"
            usernameError = false;
            return false;
        } else {
            $("#userError").hide();
            $("#userPass").show();
            document.getElementById('username').style.borderColor = "grey"
            return true;
        }
    }

    $("#emailError").hide();
    $("#emailPass").hide();
    let mailError = true;
    $("#emailError").keyup(function () {
        validateEmail();
    });


    function validateEmail() {
        let emailValue = $("#email").val();
        if (emailValue.length == "" ||
            !emailValue.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) {
            $("#emailError").show();
            $("#emailPass").hide();
            document.getElementById('email').style.borderColor = "red"
            mailError = false;
            return false;
        } else {
            $("#emailError").hide();
            $("#emailPass").show();
            document.getElementById('email').style.borderColor = "grey"
            return true;
        }
    }


    $("#positionError").hide();
    $("#positionPass").hide();
    let posError = true;
    $("#positionError").keyup(function () {
        validatePosition();
    });

    function validatePosition() {
        var position = document.getElementById('position');
        var invalid = position.value == "empty";

        if (invalid) {
            $("#positionError").show();
            $("#positionPass").hide();
            document.getElementById('position').style.borderColor = "red"
            posError = false;
            return false;
        } else {
            $("#positionError").hide();
            $("#positionPass").show();
            document.getElementById('position').style.borderColor = "grey"
            return true;
        }
    }


    $("#timeError").hide();
    $("#timePass").hide();
    let typeError = true;
    $("#timeError").keyup(function () {
        validateTime();
    });
    function validateTime() {
        let timeValue = $("#time").val();
        if (part.checked == false && full.checked == false) {
            $("#timeError").show();
            $("#timePass").hide();
            $("#timeError").html("You must select a type");
            $("#timeError").css("color", "red");
            typeError = false;
            return false;
        } else {
            $("#timeError").hide();
            $("#timePass").show();
            return true;
        }
    }

    $("#submit").click(function () {
        validateUsername();
        validateEmail();
        validatePosition();
        validateTime();
        if (
            validateUsername() == true &&
            validateEmail() == true &&
            validatePosition() == true &&
            validateTime() == true
        ) {
            $("#form").css('width', 270);
            return true;
        } else {
            $("#form").css('width', 500);
            return false;
        }
    });
});